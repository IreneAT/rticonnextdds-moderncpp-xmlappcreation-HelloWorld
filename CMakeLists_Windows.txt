cmake_minimum_required (VERSION 2.6)
project (DynamicHelloWorld)

set (ARCH "i86Win32VS2015" CACHE STRING "user_config")
set (SOURCE_DIR "src")
set (NDDSHOME $ENV{NDDSHOME})
set (COMPILER_FLAGS -Ob0 -std=c++11;-DWIN32;-DRTI_WIN32;-D_DEBUG;-D_CONSOLE;-DRTI_STATIC;-D_SCL_SECURE_NO_WARNINGS CACHE STRING "")
set (LINKER_FLAGS "" CACHE STRING "")
set (SOURCE_DIR "src")
set (EXTERNAL_LIB netapi32.lib;advapi32.lib;user32.lib;WS2_32.lib CACHE STRING "")

include_directories(${SOURCE_DIR} ${NDDSHOME}/include ${NDDSHOME}/include/ndds ${NDDSHOME}/include/ndds/hpp)
link_directories(${NDDSHOME}/lib/${ARCH})

file(GLOB SOURCES
    ${SOURCE_DIR}/*.cpp
)

add_definitions(${COMPILER_FLAGS})
add_executable(DynamicHelloWorld ${SOURCES})
target_link_libraries(DynamicHelloWorld ${LINKER_FLAGS} ${EXTERNAL_LIB} nddscpp2zd nddsczd nddscorezd)
