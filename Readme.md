Dynamic Hello World
===================

Introduction
------------

This project is a Hello World Application using DynamicData and XML Aplication Creation in C++11.

The publisher will publish samples every second and the subscriber will print the received samples.

You can change the HelloWorldDynamic.xml configuration file to select the domain where you want to run the application.

Build
-----

CMake is used to build the project.

The CMake file (CMakeList.txt) is prepared for Mac OX but could be changed easily to run in other OS.

To build the code in Darwin you should perform the following steps:

* $ cd ${ProjectWorkingFolder}
* $ mkdir build
* $ cd build
* $ cmake ../.
* $ make

Run
---

To run a publisher:
* $ cd ${ProjectWorkingFolder}
* $ build/DynamicHelloWorld pub -v

To run a subscriber:
* $ cd ${ProjectWorkingFolder}
* $ build/DynamicHelloWorld sub -v

To get some help:
* $ cd ${ProjectWorkingFolder}
* $ build/DynamicHelloWorld -h