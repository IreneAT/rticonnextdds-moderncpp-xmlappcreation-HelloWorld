/*****************************************************************************/
/*         (c) Copyright, Real-Time Innovations, All rights reserved.        */
/*                                                                           */
/*         Permission to modify and use for internal purposes granted.       */
/* This software is provided "as is", without warranty, express or implied.  */
/*****************************************************************************/
#include <string>
#ifndef __BASIC_LOGGER__
#define __BASIC_LOGGER__
#include <string>
#include <iostream>
#include <sstream>

/**
 * @brief Basic Logger that allows to write in console error and debug messages
 * 
 * The debug messages will be shown only if the print value passed in the constructor call
 * is set to true.
 * 
 */
class BasicLogger {
private:
    enum class Mode {DEBUG_LOG, ERROR_LOG};
    bool print;
    std::string className;

    template<typename T, typename ...Targs>
    void printParams(Mode type, std::stringstream& message, T value, Targs... args){
        message << value;
        printParams(type, message, args...);
    }

    void printParams(Mode type, std::stringstream& message){
        if (type==Mode::DEBUG_LOG && print){
            std::cout<<message.str()<<std::endl;
        } else if (type==Mode::ERROR_LOG){
            std::cerr<<message.str()<<std::endl;
        }
    }

public:
    /**
     * @brief Construct a new Basic Logger object
     * 
     * @param className the name of the class that will use the specific instance
     * @param print setting this attribute to true will shown the debug messages. False by default.
     */
    BasicLogger(std::string className, bool print=false):print(print), className(className){

    }

    /**
     * @brief Log the message passed as a parameter in DEBUG mode
     * 
     * This method allows to pass a list of printable objects.
     * All these objects will be printed after the message.
     * 
     * @param message the message to be printed
     * @param args the list of the object to be printed after the message
     */
    template<typename ...Targ>
    void logDebug(std::string message, Targ... args){
        std::stringstream smessage;
        smessage << "["<<className<<"] DEBUG - " << message;
        printParams(Mode::DEBUG_LOG, smessage, args...);
    }

    /**
     * @brief Log the message passed as a parameter in ERROR mode
     * 
     * This method allows to pass a list of printable objects.
     * All these objects will be printed after the message.
     * 
     * @param message the message to be printed
     * @param args the list of the object to be printed after the message
     */
    template<typename ...Targ>
    void logError(std::string message, Targ... args){
         std::stringstream smessage;
         smessage << "["<<className<<"] ERROR - " << message;
         printParams(Mode::ERROR_LOG, smessage, args...);
    }
};
#endif //__BASIC_LOGGER__