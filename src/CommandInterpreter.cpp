/*****************************************************************************/
/*         (c) Copyright, Real-Time Innovations, All rights reserved.        */
/*                                                                           */
/*         Permission to modify and use for internal purposes granted.       */
/* This software is provided "as is", without warranty, express or implied.  */
/*****************************************************************************/
#include <CommandInterpreter.hpp>
#include <iostream>

bool CommandInterpreter::get_next_argument(int argc, const char **argv, int *idx, std::string& argument){
    if (*idx + 1 >= argc) {
        logger.logError
		( "! Error: missing value for argument" );
        usage();
        return false;
    }

    argument = std::string(argv[*idx + 1]);
    (*idx)++;

    return true;
}
bool CommandInterpreter::get_next_argument(int argc, const char **argv, int *idx, int *num, int min, int max){
    std::string next_arg;
    if (!get_next_argument(argc, argv, idx, next_arg)) {
        return false;
    }

    char *ptr = NULL;
    *num = strtol(next_arg.c_str(), &ptr, 10);
    if (*ptr != '\0') {
        usage();
        logger.logError("! Argument is not a number: ", next_arg );
        return false;
    }

    if (*num < min || *num > max) {
        usage();
        logger.logError( "! Invalid argument: Number is in interval ["
                  , min , ", " , max , "]");
        return false;
    }

    return true;

}

void CommandInterpreter::usage()
{
    std::cout << "Usage:" << std::endl;
    std::cout << "    " << "Hello"
                        << " pub [arguments]     Run as publisher" << std::endl;
    std::cout << "    " << "Hello"
                        << " sub [arguments]     Run as subscriber" << std::endl;
    std::cout << "Where arguments are:" << std::endl;
    std::cout << "  -h | --help                 "
              << "Shows this page" << std::endl;
    std::cout << "  -v | --verbose              "
              << "Increase output verbosity (can be repeated)" << std::endl;
    std::cout << "  -i | --id <num>           "
              << "Sets the sample id" << std::endl;
    std::cout << "  -c | --sampleCount <num>    "
              << "Sets number of samples to send/receive [default=1000]"
              << ']' << std::endl;
    std::cout << std::endl;
}


CommandLineArguments& CommandInterpreter::readArguments(){

    // Ensure there are enough arguments in the command line
    if (argc < 2) {
        usage();
        logger.logError("! Invalid number of arguments.\n","! You must specify at least running mode (pub/sub)");
        throw std::runtime_error("Invalid number of arguments");
    }

    std::string first_arg = std::string(argv[1]);
    if (first_arg == "pub") {
        options.isPub = true;
    } else if (first_arg == "sub") {
        options.isPub = false;
    } else if (first_arg == "-h" || first_arg == "--help") {
        usage();
        options.exec = false;
    } else {
        usage();
        logger.logError("! Invalid mode: '", first_arg, "'\n", "! Valid modes are only 'pub' or 'sub'");
        throw std::runtime_error("Invalid mode");
    }

    // Parse the optional arguments
    for (int i = 2; i < argc && options.exec; i++) {
        std::string current_arg = std::string(argv[i]);
        if (current_arg == "-h" || current_arg == "--help") {
            usage();
            options.exec = true;
        } else if (current_arg == "-v" || current_arg == "--verbose") {
            options.verbose = true;
        } else if (current_arg == "-i" || current_arg == "--id") {
            int min = 1;
            int max = CommandLineArguments::ID_MAX;
            if (!get_next_argument(argc, argv, &i, &options.id, min, max)) {
                throw std::runtime_error("Invalid payload size");
            }
        }  else if (current_arg == "-c" || current_arg == "--sampleCount") {
            int min = 0;
            int max = CommandLineArguments::INT_MAX_Irene;
            if (!get_next_argument(argc, argv, &i, &options.sampleCount, min, max)) {
                throw std::runtime_error("Invalid sample count");
            }
        } else {
            usage();
            logger.logError( "! Unknown argument ", current_arg);
            throw std::runtime_error("Unknown argument");
        }
    }

    return options;

}
