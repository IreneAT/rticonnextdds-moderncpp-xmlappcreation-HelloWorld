/*****************************************************************************/
/*         (c) Copyright, Real-Time Innovations, All rights reserved.        */
/*                                                                           */
/*         Permission to modify and use for internal purposes granted.       */
/* This software is provided "as is", without warranty, express or implied.  */
/*****************************************************************************/
#include <string>
#include<BasicLogger.hpp>
#ifndef __COMMAND_INTERPRETER__
#define __COMMAND_INTERPRETER__
struct CommandLineArguments {
    static const int DOMAIN_ID_MAX = 250;
    static const int ID_MAX = 256;
    static const int INT_MAX_Irene = 10000;

    bool isPub;
    int id;
    int sampleCount;
    bool verbose;
    bool exec;

    CommandLineArguments()
    {
        isPub = false;
        id = 1;
        sampleCount = 1000;
        verbose = false;
        exec = true;
    }
};

class CommandInterpreter {
private:
    CommandLineArguments options;
    BasicLogger logger;
    int argc;
    const char **argv;

    bool get_next_argument(int argc, const char **argv, int *idx, std::string& argument);
    bool get_next_argument(int argc, const char **argv, int *idx, int *num, int min, int max);
    void usage();


public:
    CommandInterpreter(int argcp, const char **argvp):argc(argcp),argv(argvp),logger("CommandInterpreter"){}

    CommandLineArguments& readArguments();
};
#endif //__COMMAND_INTERPRETER__