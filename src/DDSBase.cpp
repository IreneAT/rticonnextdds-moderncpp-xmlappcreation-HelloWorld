/*****************************************************************************/
/*         (c) Copyright, Real-Time Innovations, All rights reserved.        */
/*                                                                           */
/*         Permission to modify and use for internal purposes granted.       */
/* This software is provided "as is", without warranty, express or implied.  */
/*****************************************************************************/

#include <DDSBase.hpp>

DDSBase::DDSBase(std::string participantProfile, bool verbose) : logger("DDSBase",verbose)
{
    rti::core::QosProviderParams qosParams;

    //Load the XML with the information related to the DDS entities
    //TODO: change the hardcode string for an attribute
    qosParams.url_profile(dds::core::StringSeq(1,"HelloWorldDynamic.xml"));
    dds::core::QosProvider::Default()->default_provider_params(qosParams);

    //Use this variable to change the configuration already load from the file via code
    rti::domain::DomainParticipantConfigParams dpConfigParams;

    logger.logDebug("Creating the Domain Participant");

    //TODO:: change the hardcode string for an attribute
    participant.reset(new dds::domain::DomainParticipant(dds::core::QosProvider::Default()->create_participant_from_config(participantProfile)));

    logger.logDebug("Creating the HelloWorld DynamicType");
    //Create the dynamic type
    //TODO:: change the hardcode string for an attribute
    dynamicType.reset(new dds::core::xtypes::DynamicType(dds::core::QosProvider::Default()->type(dds::core::QosProvider::Default()->type_libraries().at(0),"HelloWorld")));

}
