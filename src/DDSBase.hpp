/*****************************************************************************/
/*         (c) Copyright, Real-Time Innovations, All rights reserved.        */
/*                                                                           */
/*         Permission to modify and use for internal purposes granted.       */
/* This software is provided "as is", without warranty, express or implied.  */
/*****************************************************************************/
#ifndef __DDS_BASE__
#define __DDS_BASE__

#include <dds/dds.hpp>
#include <memory>
#include <string>

#include <BasicLogger.hpp>

/**
 * @brief This class will contain the DDS Domain Participant and the Dynamic Type to be used for publishing and subscribing data
 * 
 */
class DDSBase {
protected:
    std::unique_ptr<dds::domain::DomainParticipant> participant;
    std::unique_ptr<dds::core::xtypes::DynamicType> dynamicType;
    BasicLogger logger;


public:
    DDSBase(std::string participantProfile, bool verbose);
};

#endif //__DDS_BASE__