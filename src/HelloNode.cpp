
/*****************************************************************************/
/*         (c) Copyright, Real-Time Innovations, All rights reserved.        */
/*                                                                           */
/*         Permission to modify and use for internal purposes granted.       */
/* This software is provided "as is", without warranty, express or implied.  */
/*****************************************************************************/
#include <HelloNode.hpp>
#include <HelloPublisher.hpp>
#include <HelloSubscriber.hpp>
#include <iostream>
#include <dds/dds.hpp>
#include <rti/config/Version.hpp>

void HelloNode::start_application(const CommandLineArguments &options)
{
    std::cout <<
        "# The output below depends on the QoS profile"        << std::endl <<
        "# provided to this application."                      << std::endl <<
        "# -> For more information on the provided example"    << std::endl <<
        "#    profiles, please see the Getting Started Guide." << std::endl <<
        "# -> For detailed product performance metrics, visit" << std::endl <<
        "#    http://www.rti.com/products/data_distribution/index.html" << std::endl <<
        "#    and click on Benchmarks."           << std::endl << std::endl;

    if (options.isPub) {
        try {
            HelloPublisher publisher(options.verbose);
            publisher.publish(options.id, options.sampleCount);
        } catch (const std::exception& ex) {
            std::cerr << "Error creating publisher: " << ex.what() << std::endl;
        }
    } else {
        try {
            HelloSubscriber subscriber(options.verbose);
            subscriber.receive(options.sampleCount);
        } catch (const std::exception& ex) {
            std::cerr << "Error creating subscriber: " << ex.what() << std::endl;
        }
    }
}


int main(int argc, const char **argv)
{
    try{
    std::cout << "Hello Example Application" << std::endl
              << "Copyright 2017 Real-Time Innovations, Inc." << std::endl
              << std::endl;
    CommandInterpreter commandInterpreter(argc, argv);
    CommandLineArguments options = commandInterpreter.readArguments();
    if (!options.exec) {
        return 1;
    }

    if (options.verbose) {
        std::cout << "Running with the following arguments:" << std::endl;
        std::cout << "    Sample id..... : " << options.id << std::endl;
        std::cout << "    Sample count..... : " << options.sampleCount << std::endl;
        std::cout << "RTI Product Version.. : "
                  << rti::config::product_version().to_string() << std::endl;
    }

    HelloNode::start_application(options);
    dds::domain::DomainParticipant::finalize_participant_factory();

    return 0;
    } catch (...){
        return -1;
    }
}