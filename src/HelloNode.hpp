/*****************************************************************************/
/*         (c) Copyright, Real-Time Innovations, All rights reserved.        */
/*                                                                           */
/*         Permission to modify and use for internal purposes granted.       */
/* This software is provided "as is", without warranty, express or implied.  */
/*****************************************************************************/
#ifndef __HELLO_NODE__
#define __HELLO_NODE__
#include <CommandInterpreter.hpp>
class HelloNode{
private:
    HelloNode(){}
public:
    
     static void start_application(const CommandLineArguments& options);

};
#endif //__HELLO_NODE__