/*****************************************************************************/
/*         (c) Copyright, Real-Time Innovations, All rights reserved.        */
/*                                                                           */
/*         Permission to modify and use for internal purposes granted.       */
/* This software is provided "as is", without warranty, express or implied.  */
/*****************************************************************************/
#include<HelloPublisher.hpp>
#include <sstream>
#include <dds/dds.hpp>
  
HelloPublisher::HelloPublisher(bool verbose) : DDSBase("MyParticipantLibrary::MyPublicationParticipant", verbose)
{
}

void HelloPublisher::publish(int id, int sampleCount){

    logger.logDebug("Creating the DW");
    //TODO:: change the hardcode string for an attribute
    dds::pub::DataWriter<dds::core::xtypes::DynamicData> dw = 
        rti::pub::find_datawriter_by_name<dds::pub::DataWriter<dds::core::xtypes::DynamicData>>(*participant, "MyPublisher::MyHelloWorldWriter");

    logger.logDebug("Creating the sample");
    dds::core::xtypes::DynamicData sample(*dynamicType);
    sample.value("senderId",id);

    // Write samples from the instance
    logger.logDebug("Sending Data");
    for (int i=0;i<sampleCount;i++){

        //Change the message field of the sample
        std::stringstream ss;
        ss << "Hello world, " << i;
        sample.value<std::string>("message", ss.str());

        logger.logDebug("Writing a new sample ", sample);
        dw.write(sample);
        //TODO:: change the hardcode time for an attribute
        rti::util::sleep(dds::core::Duration::from_millisecs(1000));
    }

}