/*****************************************************************************/
/*         (c) Copyright, Real-Time Innovations, All rights reserved.        */
/*                                                                           */
/*         Permission to modify and use for internal purposes granted.       */
/* This software is provided "as is", without warranty, express or implied.  */
/*****************************************************************************/

#ifndef __HELLO_PUBLISHER__
#define __HELLO_PUBLISHER__

#include <DDSBase.hpp>
#include <string>

/**
 * @brief This class will create a DDS Hello world publisher
 * 
 * It will publish a sample per second when calling publish method.
 * 
 */
class HelloPublisher : public DDSBase {

public:
    /**
     * Creates the DDS entities for the publisher.
     * @param verbose the verbose level.
     */
    HelloPublisher(bool verbose);

    /**
     * Publisher destructor
     */
    ~HelloPublisher(){}

    /**
     * Start sending the data.
     * @param id the id of the sample to be sent.
     * @param sampleCount the number of samples to be sent.
     */
    void publish(int id, int sampleCount);

};

#endif