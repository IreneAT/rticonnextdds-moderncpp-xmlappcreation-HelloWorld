/*****************************************************************************/
/*         (c) Copyright, Real-Time Innovations, All rights reserved.        */
/*                                                                           */
/*         Permission to modify and use for internal purposes granted.       */
/* This software is provided "as is", without warranty, express or implied.  */
/*                                                                           */
/*****************************************************************************/
#include<HelloSubscriber.hpp>
#include <dds/dds.hpp>

HelloSubscriber::HelloSubscriber(bool verbose) : DDSBase("MyParticipantLibrary::MySubscriptionParticipant", verbose){

}

void HelloSubscriber::receive(int sampleCount){
    
    logger.logDebug("Creating the Data Reader");
    dds::sub::DataReader<dds::core::xtypes::DynamicData> dr = 
        rti::sub::find_datareader_by_name<dds::sub::DataReader<dds::core::xtypes::DynamicData> >(*participant, "MySubscriber::MyHelloWorldReader");

    int i=0;
    //Create the functor that processes the samples received
    auto functor = [&dr,&i]()
        {
            rti::sub::LoanedSamples<dds::core::xtypes::DynamicData> samples = dr.take();

            for(auto sample:samples){
                auto data = sample.data();
                std::cout<<"Received a new sample: \n" << data << std::endl;
                i++;
            }
        };

    logger.logDebug("creating the read Condition");
    dds::sub::cond::ReadCondition condition(dr,
                dds::sub::status::DataState(dds::sub::status::SampleState::not_read(),
                                            dds::sub::status::ViewState::any(), 
                                            dds::sub::status::InstanceState::alive()), functor);
    logger.logDebug("Creating the waitset");
    dds::core::cond::WaitSet waitSet;
    waitSet.attach_condition(condition);

    logger.logDebug("entering the loop");
    while (i<sampleCount){
        // Wait at most 20 seconds until one or more conditions are active and
        // then call the handler of the active conditions
        waitSet.dispatch(dds::core::Duration::from_secs(20));
    }
}
