/*****************************************************************************/
/*         (c) Copyright, Real-Time Innovations, All rights reserved.        */
/*                                                                           */
/*         Permission to modify and use for internal purposes granted.       */
/* This software is provided "as is", without warranty, express or implied.  */
/*                                                                           */
/*****************************************************************************/

#ifndef __HELLO_SUBSCRIBER__
#define __HELLO_SUBSCRIBER__

#include<DDSBase.hpp>
#include <string>

/**
 * @brief This class will create a DDS Hello world subscriber
 * 
 * It will receive samples in a loop.
 * 
 */
class HelloSubscriber : public DDSBase {

public:
    /**
     * Creates the DDS entities for the subscriber.
     * @param verbose if the debug messages will be shown.
     */
    HelloSubscriber(bool verbose);

    /**
     * Subscriber destructor
     */
    ~HelloSubscriber(){}

    /**
     * Start receiving the data.
     * 
     * @param sampleCount number of samples to receive before exit
     */
    void receive(int sampleCount);

};

#endif